//External JS

// alert('Hello World');

 // Writing Comments

//single line comment - cmd + /
 /* multi-line comment - cmd + opt + /
asdad
dsfsf 
dsfsdfdsfsdf
dsfdsf
*/


/*let js = "challenging";

if (js === "challenging"){
	alert('Javascript is challenging');
}*/


// Variables and Constants

	//variable is a container that holds data/ value
		//value - a data that you assign to a variable

	let myName = "Joy";
	console.log(myName);

	// var / let

	let student;			//Variable declaration using var/let keyword
	console.log(student);		//undefined

	//Veriable Declaration & Initialization
	let yourName = "Marge";
	console.log(yourName);

	//Initialized value to variable student
		//-assigns value to a variable
	student = "Marge";
	console.log(student);

	//Q: Can i change the value of the variable myName to a new value? - YES

	// re-assigning of value
	myName = "Johann";
	console.log(myName);

	myName	= "Nate";
	console.log(myName);

	// let myName = "Aero";
	// console.log(myName);

	var lastName = "Pague";
	console.log(lastName);

	//in between we have thousands of codes

	var	lastName = "Escano";
	console.log(lastName);



	//Another type of variable is Constant
		//Constant
			//-has fixed value, it can never be re-assogned with a new value

	const myJob = "developer";
	console.log(myJob);

	// myJob = "instructor";
	// console.log(myJob);

	//Q: Can we declare a constant without initializing a value? -NO

	// const coding;
	// console.log(coding);

	// Q: Why is variable important?
			// - it is reusable

	let brand = "mac";

	brand = "msi";


	console.log("brand");
	console.log("brand");
	console.log("brand");
	console.log("brand");
	console.log("brand");
	console.log("brand");

	//Scenarios of variables without values

	// mylastName;
	// console.log(mylastName);	//ReferenceError: mylastName is not defined

	let myFirstName;
	console.log(myFirstName);	//undefined

	//variable naming conventions

		// case sensitive

		let color = "pink";

		let Color = "blue";

		console.log(color);
		console.log(Color);

		//variable should describe its value

		let value = "Robin";

		let val = "Robin";

		let hisName = "Robin";

		//camel case notation
			//first letter is lowercase, 
			// succeding words' first letter is uppercase

		let capitalCityOfThePhilippines = "Manila";

		//numbers before letters are not allowed

		// let 3year = 3;
		// console.log(3year);

		let year3 = 3;
		console.log(year3);

		let year4 = "3";
		console.log(year4);

		// allowed

		let _year5 = 5;
		console.log(_year5);

		let $year = 6;
		console.log($year);
	//Q: Is it possible to use a single quotation mark?
	let single = 'single';
	console.log(single);

	let double = "single";
	console.log(double);

	let backtick = `backticks`;
	console.log(backtick);

	let sheSaid = `She said, "Hi Joy!"`;
	console.log(sheSaid);

	sheSaid = "She said, \"Hi Joy!\"";
	console.log(sheSaid);


	let favoriteName = `Jomar`;

	sheSaid = `She said, "Hi ${favoriteName}!"`;
	console.log(sheSaid);

//constant naming convention
	// constant variable names be written in capital letters for one word
	const PI = 3.14;
	console.log(PI);

	const SECONDS = 60;
	const MINUTES = 60;
	const HOURS = 24;

	// if more than one word, use came case notation
	const boilingPointOfWater = 100;
	const birthYear = 1993;
	const JOB = `Web Developer`;
	console.log(JOB);

/* Mini Activity */

	let country = `Philipines`;
	console.log(country);

	let continent = `Asia`;
	console.log(continent);

	let philippinePopulation =`109.6 million`
	console.log(philippinePopulation)

	
	console.log(`Our country ${country}`);
	//template literal
		//backticks
		//${}

/*Data Types */


//String
	//sequence of characters
	//always wrapped in quotes or template literals
let flower = `Tulips`;
console.log(flower);

let mobileNo = `+639177025807`
console.log(mobileNo);

//if no quotes or backticks, JS will confuse them with variable names
let Manila = `Ermita`

let city = `Manila`;
console.log(city);

//integers or Numbers
	//whole number (integer) & decimals (float)
	// always so-called floating point numbers
			// means that they always have decimals even if we dont
			//see it or dont define it
let age = 28;
console.log(typeof age); //number

let myAge = 30.00;
let hisAge = 30;

console.log(myAge == hisAge);

//Boolean - true/false
	//logical type that can only be true or false

let isEarly = true;	
console.log(typeof isEarly); //boolean

// let isEarly = `true`;
// console.log(typeof isEarly);  //string

let areLate = false;


console.log(isEarly === isEarly);

	//naming convention of Boolean;
		// -a a prefix like: is, are, has, have
		//JS developers use these prefixes to distinguish 
		// a boolean from another variable by just looking at it

/* typeof operator
	- checks what kind of data type a variable has

	syntax:

	typeof <variable>;
*/

//Undefined 
	//empty value but can be assigned with a new value
	//declared but no value initialized
let children;
console.log(typeof children); //undefined


//siblings;
//console.log(siblings); // not defined - never been declared 
//anywhere in javascript file

//Null
	//"empty value"
	// it is an ASSIGNED VALUE to a variable that is
	//used to represent an empty value
let spouse = null;
console.log(spouse);

console.log(typeof null); // object

//BigInt()
	// - large numbers than the number data type can hold


// Object 
	//one variable but it contains several different 
	//types of data
let person = {
	//property: value
	name: "Jose Rizal",
	age: 30,
	address: "Calamba, Laguna",
	isDoctor: true,
	spouse: null,
	//array
	siblings: [
	"Paciano", 
	"Josefa", 
	"Olympia", 
	"Saturnina", 
	"Concepcion",
	"Soledad", 
	"Maria",
	]
}

console.log(typeof person);

//* special type of Object - Array **

let grades = [
92,
94,
96,
98
]

console.log(grades); //object
console.log(typeof grades);

/* Function */
	// it is a reusable code to prevent duplication

//===ANATOMY OF A FUNCTION
//Function Declaration
	//function keyword
	//function name & parenthesis
		//- parenthesis holds the parameters
			//- parenthesis hold the arguments (actual value of the parameter)
	//curly braces
		// -to determine its blocks of codes
		// -statements are written inside the code block
function sayHelloWorld(){
	//statement
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
	console.log("Hello World!");
}

//Function Invocation
	//function name & parenthesis
// sayHelloWorld(); // first set
// sayHelloWorld(); //second set

//Another example of a function:
		//parameters
function greeting(firstName, lastName) {
	console.log(`Hi ${firstName} ${lastName}! How are you?`)
}
		//arguments
greeting("Joy", "Pague");
greeting("Marge", "Escano");
greeting("Johann", "Bleza");
greeting("Kaiser", "Tabuada");


/* Mini Activity */

function add(a, b, c){
	return a + b + c;
}

console.log(1+2+3)


function totalSum(num1, num2, num3) {
	console.log(`The sum of three numbers 
		${num1} + 
		${num2} + 
		${num3} = 
		${num1 + num2 + num3}`)
}

totalSum(4, 8, 12);
totalSum(100, 200, 300);


function totalDifference(num1, num2, num3) {
	return(`The sum of three numbers 
		${num1} + 
		${num2} + 
		${num3} = 
		${num1 + num2 + num3}`)
}

function product(a, b) {
	console.log(`The product of two numbers are:`)
	return (
		a * b
	)
	
}

console.log(product(2, 4));








