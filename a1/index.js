///index.js

let greeting = `Hello World`;
console.log(greeting);


let firstName = `John`;
console.log(`First Name: ${firstName}`)



let lastName = `Smith`;
console.log(`Last Name: ${lastName}`)

let age = 30;
console.log(`Age: ${age}`)

let hobbies = [
'Biking',
`Mountain Climbing`,
`Swimming`
]
console.log(`Hobbies:`)
console.log(hobbies);

let workAddress = `Work Address:`;
console.log(`Work Address:`)
console.log()


let address = {
	houseNumber: 32,
	street: `Washington`,
	city: `Lincoln`,
	state:`Nebraska`,
}

console.log(address)

console.log(`${firstName} ${lastName} is ${age} years of age.`)
console.log(`This was printed inside printUSerInfo function:`)

function printUserInfo(hobbies){
	return hobbies
}
 
console.log(hobbies)

console.log(`This was printed inside printUSerInfo function:`)

function printUserInfo(address){
	return address
}

console.log(address) 

let isMarried = true;
console.log(`The value of isMarried is: ${isMarried}`)
